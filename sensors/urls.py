"""Django urls for the sensors app."""

from django.urls import path

from . import views

app_name = 'sensors'
urlpatterns = [
    path('index/', views.IndexView.as_view(), name='landing_index'),
    path('', views.SensorsList.as_view(), name='sensors_index'),
    path('<int:pk>/', views.SensorDetailView.as_view(), name='sensor_detail'),
    path('probe/', views.ProbesList.as_view(), name='probes_index'),
    # views for station
    path('station/', views.MeasuringStationList.as_view(), name='stations_index'),
    path('station/<int:pk>/', views.MeasuringStationDetailView.as_view(), name='station_detail'),
    path('station/<int:pk>/edit', views.MeasuringStationEditView.as_view(), name='station_update'),
    path('station/<int:pk>/delete/', views.MeasuringStationDeleteView.as_view(), name='station_delete'),
    path('station/add/', views.MeasuringStationCreateView.as_view(), name='station_create'),
]
