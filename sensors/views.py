"""Django views for the sensors app."""

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views import generic
from .models import Sensor, Measure, ProbeDriver, MeasuringStation
from django.utils import timezone
from .forms import AddMeasuringStationForm, EditMeasuringStationForm
from django.views.generic.edit import UpdateView, DeleteView, CreateView


class SensorsList(generic.ListView):
    """Show the list of saved sensors."""

    paginate_by = 10
    model = Sensor
    template_name = 'sensors/sensors_list.html'
    context_object_name = 'latest_sensor_list'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['sensors_count'] = Sensor.objects.all().count()
        return context

    def get_queryset(self):
        """Return the last 20 configured sensors."""
        return Sensor.objects.order_by('id')[:20]


class MeasuringStationList(generic.ListView):
    """Show the list of measuring stations."""

    paginate_by = 10
    model = MeasuringStation
    template_name = 'sensors/measuringstations_list.html'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['count'] = MeasuringStation.objects.all().count()
        return context


class MeasuringStationDetailView(generic.DetailView):
    """Show the details of a specific measuring station."""

    model = MeasuringStation
    template_name = 'sensors/measuringstation_detail.html'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['mode'] = "view"
        return context


class MeasuringStationEditView(UpdateView):
    """Edit a specific measuring station."""

    model = MeasuringStation
    fields = [
        "name",
        "description",
        "installed_sensors"
    ]
    success_url = "#"


class MeasuringStationDeleteView(DeleteView):
    """Delete a specific measuring station."""

    model = MeasuringStation
    success_url = reverse_lazy('sensors:stations_index')


class MeasuringStationCreateView(CreateView):
    """Create a new measuring station."""

    model = MeasuringStation
    fields = [
        "name",
        "description",
        "installed_sensors"
    ]

    def get_success_url(self):
        msg = "Measuring station successfully created. Details below."
        return reverse_lazy('sensors:stations_index')
    #success_url = reverse_lazy('sensors:station_detail', args=(self.object.id))


class ProbesList(generic.ListView):
    """Show the list of saved probe_drivers."""

    paginate_by = 10
    model = ProbeDriver
    template_name = 'sensors/probes_list.html'
    context_object_name = 'latest_probes_list'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['probes_count'] = ProbeDriver.objects.all().count()
        return context

    def get_queryset(self):
        """Return the last 20 probes."""
        return ProbeDriver.objects.order_by('name')[:20]


class SensorDetailView(generic.DetailView):
    """Show the details of a specific sensor."""

    model = Sensor
    template_name = 'sensors/sensor_detail.html'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['last'] = Measure.objects.filter(sensor_id=context['sensor'].id).order_by("-ts")[0]
        context['probe'] = ProbeDriver.objects.get(name=context['sensor'].probe_driver)
        context['measures_count'] = Measure.objects.filter(sensor_id=context['sensor'].id).count()
        return context


class IndexView(generic.ListView):
    """View for the main landing page of the app."""

    template_name = 'sensors/index.html'
    context_object_name = 'latest_sensor_list'

    def get_context_data(self, **kwargs):
        """Get the context for the view, generic."""
        context = super().get_context_data(**kwargs)
        context['sensors_count'] = Sensor.objects.all().count()
        return context

    def get_queryset(self):
        """Return the last four saved sensors."""
        return Sensor.objects.order_by('-id')[:4]
