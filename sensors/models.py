"""Django models for the sensors app."""

from django.db import models


class ProbeDriver(models.Model):
    """Describe how probe drivers are structured.

    A probe driver describe the physical sensor and it's capacities.
    For multi sensors, such as the bme280, that outputs multiple variables,
    multiple probe sensors are created, one for each available measurement.
    """

    name = models.CharField('name of the driver', max_length=255,
                            primary_key=True)
    description = models.CharField('descripton of the driver', max_length=2048,
                                   blank=True)
    unit = models.CharField('measurement unit', max_length=255, blank=True)
    unit_symbol = models.CharField('symbol of the unit', max_length=255,
                                   blank=True)
    min_value = models.FloatField('lowest possible value', blank=True,
                                  null=True)
    max_value = models.FloatField('highest possible value', blank=True,
                                  null=True)

    def __str__(self):
        """Return a string representation of the object."""
        return self.name


class Sensor(models.Model):
    """Describe how Sensors are structured.

    A sensor in this context is when a probe driver is used with
    specific parameters set by the user. It is not about the generic sensor,
    but rather about how the user wants to collect it's data.
    """

    id = models.IntegerField(primary_key=True)
    name = models.CharField('user friendly name', max_length=255)
    description = models.CharField('description', max_length=2048, blank=True)
    probe_id = models.CharField('unique ID of the probe', max_length=1024,
                                default="0x00", blank=True)
    probe_driver = models.ForeignKey(ProbeDriver, on_delete=models.CASCADE)

    def __str__(self):
        """Return a string representation of the object."""
        return self.name


class Measure(models.Model):
    """Describe how measures are structured.

    A measure is taken by a sensor of a defined type, and
    is saved in database, using the current timestamp at the
    time of insertion. The value is whatever the sensor returned.
    timestamp (ts) and the sensor id (sensor_id) are indexed for
    faster access times on reads.
    """

    id = models.AutoField(primary_key=True)
    sensor_id = models.ForeignKey(
                                  Sensor,
                                  on_delete=models.CASCADE,
                                  db_index=True
                                 )
    value = models.FloatField()
    ts = models.DateTimeField(db_index=True)

    def __str__(self):
        """Return a string representation of the object."""
        return "value: " + str(self.value)

    def was_published_recently(self):
        """Tell if the measure was saved in the last day."""
        return self.ts >= timezone.now() - datetime.timedelta(days=1)


class MeasuringStation(models.Model):
    """Describe how measuring stations are structured.

    A measuring station is a network device that have access to sensors,
    and periodically access those sensors to retrieve the values and store them
    into the database.
    """

    id = models.AutoField(primary_key=True)
    name = models.CharField('user friendly name', max_length=255)
    description = models.CharField('description', max_length=2048, blank=True)
    installed_sensors = models.ManyToManyField(Sensor)

    def __str__(self):
        """Return a string representation of the object."""
        return self.name
