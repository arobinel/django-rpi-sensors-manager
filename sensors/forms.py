from django import forms
from .models import Sensor, MeasuringStation


class AddMeasuringStationForm(forms.Form):
    station_name = forms.CharField(label='station name', max_length=255)
    station_description = forms.CharField(label='station description', max_length=255)
    installed_sensors = forms.ModelMultipleChoiceField(
            queryset=Sensor.objects,
            widget=forms.SelectMultiple,
            )


class EditMeasuringStationForm(forms.Form):
    # m = MeasuringStation.objects.get(id=station_id)
    station_name = forms.CharField(label='station name', max_length=255)
    station_description = forms.CharField(label='station description', max_length=255)
    installed_sensors = forms.ModelMultipleChoiceField(
            queryset=Sensor.objects,
            widget=forms.SelectMultiple,
            )
