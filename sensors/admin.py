from django.contrib import admin

# Register your models here.
from .models import Sensor, Measure, ProbeDriver, MeasuringStation

admin.site.register(Sensor)
admin.site.register(Measure)
admin.site.register(ProbeDriver)
admin.site.register(MeasuringStation)
