"""Django tests for the sensors app."""

import datetime
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from .models import MeasuringStation

# Create your tests here.


class TestMeasuringStationURI(TestCase):
    def test_measuring_station_list_response(self):
        client = Client()
        response = client.get(reverse('sensors:stations_index'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_measuring_station_create_response(self):
        client = Client()
        response = client.get(reverse('sensors:station_create'), follow=True)
        self.assertEqual(response.status_code, 200)



    def test_measuring_station_details_response(self):
        client = Client()
        m = MeasuringStation(name="test", description="testDescription")
        m.save()
        response = client.get(reverse('sensors:station_detail', args=(m.id,)), follow=True)
        self.assertEqual(response.status_code, 200)


    def test_measuring_station_update_response(self):
        client = Client()
        m = MeasuringStation(name="test", description="testDescription")
        m.save()
        response = client.get(reverse('sensors:station_update', args=(m.id,)), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_measuring_station_delete_response(self):
        client = Client()
        m = MeasuringStation(name="test", description="testDescription")
        m.save()
        response = client.get(reverse('sensors:station_delete', args=(m.id,)), follow=True)
        self.assertEqual(response.status_code, 200)
